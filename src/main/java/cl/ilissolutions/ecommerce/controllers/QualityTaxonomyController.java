package cl.ilissolutions.ecommerce.controllers;

import cl.ilissolutions.ecommerce.model.QualityTaxonomy;
import cl.ilissolutions.ecommerce.services.QualityTaxonomyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class QualityTaxonomyController {

    @Autowired
    QualityTaxonomyService qualityTaxonomyService;

    @GetMapping("/qualitytaxonomy/all")
    public ResponseEntity<List<QualityTaxonomy>> getAll() {
        return ResponseEntity.ok(qualityTaxonomyService.getAll());
    }

    @PostMapping("qualitytaxonomy")
    public ResponseEntity<QualityTaxonomy> save(@RequestBody QualityTaxonomy qualityTaxonomy){
        return ResponseEntity.ok(qualityTaxonomyService.save(qualityTaxonomy));
    };


}
