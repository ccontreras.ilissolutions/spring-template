package cl.ilissolutions.ecommerce.controllers;

import cl.ilissolutions.ecommerce.model.Taxonomy;
import cl.ilissolutions.ecommerce.services.TaxonomyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class TaxonomyController {

    @Autowired
    private TaxonomyService taxonomyService;

    @GetMapping("/taxonomy/{id}")
    public Optional<Taxonomy> getTaxonomy(@PathVariable Long id){
        return taxonomyService.getId(id);
    }

    @GetMapping("/all")
    public ResponseEntity<List<Taxonomy>> getAll(){
        return ResponseEntity.ok(taxonomyService.getAll());
    }

    @PostMapping()
    public ResponseEntity<Taxonomy> save(@RequestBody Taxonomy taxonomy){
        return ResponseEntity.ok(taxonomyService.save(taxonomy));
    }

    @DeleteMapping()
    public ResponseEntity delete (@RequestBody Taxonomy taxonomyRequest){
        if (taxonomyService.delete(taxonomyRequest)){
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

}
