package cl.ilissolutions.ecommerce.repository;

import cl.ilissolutions.ecommerce.model.QualityTaxonomy;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QualityTaxonomyRepository extends CrudRepository<QualityTaxonomy, Long> {

}
