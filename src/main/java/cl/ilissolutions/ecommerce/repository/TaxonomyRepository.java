package cl.ilissolutions.ecommerce.repository;

import cl.ilissolutions.ecommerce.model.Taxonomy;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaxonomyRepository extends CrudRepository <Taxonomy, Long> {
}
