package cl.ilissolutions.ecommerce.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "taxonomy")
public class Taxonomy {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "id_type")
    private String idType;

    private String vocabulary;

    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="id_taxonomy", nullable=false)
    private QualityTaxonomy quality;

}
