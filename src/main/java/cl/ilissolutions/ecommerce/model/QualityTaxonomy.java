package cl.ilissolutions.ecommerce.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name="quality_taxonomy_term")
public class QualityTaxonomy {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "id_taxonomy")
    private String idTaxonomy;

    @Column(name ="id_parent")
    private String idParent;

    private String name;

    @OneToMany(mappedBy="quality")
    private List<Taxonomy> taxonomies;

}
