package cl.ilissolutions.ecommerce.services;

import cl.ilissolutions.ecommerce.model.QualityTaxonomy;

import java.util.List;
import java.util.Optional;

public interface QualityTaxonomyService {

  List<QualityTaxonomy> getAll();

  QualityTaxonomy save(QualityTaxonomy qualityTaxonomy);
}
