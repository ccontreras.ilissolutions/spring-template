package cl.ilissolutions.ecommerce.services;

import cl.ilissolutions.ecommerce.model.Taxonomy;
import cl.ilissolutions.ecommerce.repository.TaxonomyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TaxonomyServiceImpl implements TaxonomyService {

    @Autowired
    private TaxonomyRepository taxonomyRepository;

    @Override
    public Optional<Taxonomy> getId(Long id){
        return taxonomyRepository.findById(id);
    }

    @Override
    public List<Taxonomy> getAll() {
        return (List<Taxonomy>) taxonomyRepository.findAll();
    }

    @Override
    public Taxonomy save(Taxonomy taxonomy) {
        return taxonomyRepository.save(taxonomy);
    }

    @Override
    public boolean delete(Taxonomy taxonomyRequest) {
        return getId(taxonomyRequest.getId()).map(taxonomy -> {
            taxonomyRepository.delete(taxonomyRequest);
            return true;
        }).orElse(false);
    }

    @Override
    public int testInt(){
        return 1;
    }

}
