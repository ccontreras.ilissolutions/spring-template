package cl.ilissolutions.ecommerce.services;

import cl.ilissolutions.ecommerce.model.Taxonomy;

import java.util.List;
import java.util.Optional;

public interface TaxonomyService {
    Optional<Taxonomy> getId(Long id);

    List<Taxonomy> getAll();

    Taxonomy save(Taxonomy taxonomy);

    boolean delete(Taxonomy taxonomyRequest);
    int testInt();
}
