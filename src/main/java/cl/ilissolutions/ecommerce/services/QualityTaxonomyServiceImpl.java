package cl.ilissolutions.ecommerce.services;

import cl.ilissolutions.ecommerce.model.QualityTaxonomy;
import cl.ilissolutions.ecommerce.model.Taxonomy;
import cl.ilissolutions.ecommerce.repository.QualityTaxonomyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class QualityTaxonomyServiceImpl implements QualityTaxonomyService {

    @Autowired
    public QualityTaxonomyRepository qualityTaxonomyRepository;

    @Override
    public List<QualityTaxonomy> getAll() {
        return (List<QualityTaxonomy>) qualityTaxonomyRepository.findAll();
    }

    @Override
    public QualityTaxonomy save(QualityTaxonomy qualityTaxonomy) {
        return qualityTaxonomyRepository.save(qualityTaxonomy);
    }

}
